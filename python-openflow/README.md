# README #
# python-openflow

Kytos is a fork of
https://github.com/kytos/python-openflow

fork can be located at
https://github.com/slinksoft/python-openflow

A usage, and installation guide can be found the Appendix D
of the project documentation.

We modified the following files:

- test_struct.py

Please refer to the provided installation document to set things up.