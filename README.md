# README #
# kytos 

Kytos is a fork of
https://github.com/kytos/kytos

fork can be located at
https://github.com/slinksoft/kytos

We modified the following files:

- kytosd.py
- config.py
- controller.py

# kytos-utils

Kytos is a fork of
https://github.com/kytos/kytos-utils

fork can be located at
https://github.com/slinksoft/kytos-utils


We modified the following files:

- napps.py
- decorators.py

# python-openflow

Kytos is a fork of
https://github.com/kytos/python-openflow

fork can be located at
https://github.com/slinksoft/python-openflow

We modified the following files:

- test_struct.py

A usage, and installation guide can be found the Appendix D
of the project documentation.

Please refer to the provided installation document to set things up.